﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Capita.Business.CapitaHelper;

namespace Capita.UI.ConsoleUI
{
    class Calc
    {
        static void Main()
        {
            int num1, num2,result;
            char op;
            bool choice =true;
            var utitily = new Utility();
            Console.WriteLine("Enter First Number: ");
            num1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Second Number: ");
            num2 = Convert.ToInt32(Console.ReadLine());
            
            while (choice)
            {
                Console.WriteLine("Basic Operation");
                Console.WriteLine("+ Addition");
                Console.WriteLine("- Subtraction");
                Console.WriteLine("* Multiplication");
                Console.WriteLine("/ Division");
                Console.WriteLine("e Exit");
                Console.WriteLine("Enter Operation: ");
                op = Convert.ToChar(Console.ReadLine());

                switch (op)
                {
                    case '+':
                        
                        Console.WriteLine(utitily.Sum(num1,num2));
                        
                        break;
                    case '-':
                        
                        Console.WriteLine(utitily.Sub(num1,num2));
                        
                        break;
                    case '*':
                        
                        Console.WriteLine(utitily.Mult(num1, num2));
                        
                        break;
                    case '/':
                        
                        Console.WriteLine(utitily.Div(num1, num2));
                        
                        break;
                    case 'e': choice = false;
                            break;
                    default:
                        Console.WriteLine("Enter Valid Operation");
                        
                        break;

                }
            }
            Console.ReadLine();


        }
    }
}
